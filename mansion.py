# Import the required libraries
from tkinter import ALL
from tkinter import *
from PIL import Image, ImageTk
import os
from time import sleep
from pecrs import *
import math
import random
from os.path import exists
SRAM = []
colliders = []
speed_X = 10
speed_Y = 10
debug = 1
friends = 0 # for bonus realism because i do not have friends(T.T)
syncy = 250
syncx = 250
playerx = 0
playery = 0
velx = 0
vely = -10
space = Space()
jump = 1
# setup ##############
win=Tk()
win.geometry("500x500")
canvas=Canvas(win , width=500, height=500)
canvas.pack()
collided = 323
debugtool = 0



##classes##############
class collisionbox:
    def __init__(self, x1 , y1 , x2 , y2 ):
     global space
     global xoffsetcollisionhack
     self.initialState = [[x1 , y2] , [x1 , y1] , [x2 , y1] , [x2 , y2]]
     self.bounds = (x1 , y1 , x2 , y2  )
     self.xf = x1
     self.yf = y1
     self.x1 = x2
     self.y1 = y2
     self.code = "io = 0"
     self.name = "default"
     
   
    
    
    
    
class sprite:
    def __init__(self, master , x , y , mode):
     path = file=os.getcwd()+'/assets/' + master
     self.img=ImageTk.PhotoImage(file=path)
     self.x = x
     self.y = y
     self.camx = y
     self.camy = x
     self.xtrack = x - 250
     self.ytrack = y - 250
     self.mode = mode
     self.id = canvas.create_image(self.x, self.y, image=self.img, anchor="center")
     self.code = '''io = 0 '''
     self.bounds = (50 , 50 , -50 , -50  )
     self.cx = x
     self.cy = y
     
     self.name = "default"

     canvas.scale(self.id, 250, 250, 2.0, 2.0)
    def move(self,):
       canvas.move(self.id, self.x, self.y)
    def rmove(self , x , y):
     self.cx = self.cx  + - x
     self.cy = self.cy + - y
     self.xtrack = self.xtrack +   x
     self.ytrack = self.ytrack +  y
     self.x = self.x + x
     self.y = self.y + y
   
    def ghost(self):
        global collided
        self.xb = self.x
        self.yb = self.y
        self.rmove(1 , 0)
        self.collbox = collisionbox( self.cx + self.bounds[2] , self.cy + self.bounds[3] , self.cx + self.bounds[0] , self.cy + self.bounds[1] )
        if collisioncheckworld(self.collbox):
            self.collisions = collided
            self.x = self.xb
            self.y = self.yb
    def fkcollbox(self):
     print(self.collbox.bounds) 
    def collideswithworld(self):
     playercollbox = collisionbox( self.cx + self.bounds[2] , self.cy + self.bounds[3] , self.cx + self.bounds[0] , self.cy + self.bounds[1] )
     print( self.xtrack + self.bounds[2] , self.ytrack + self.bounds[3] , self.xtrack + self.bounds[0] , self.ytrack + self.bounds[1] )
     global colliders
     total_collisions = 0
     self.collided = []
     for ActiveSprite in range(-1, len(colliders) - 1):
        if  collisioncheck( playercollbox , colliders[ActiveSprite]):
             total_collisions = total_collisions + 1
             self.collided.append(str(colliders[ActiveSprite].name))
             print("ICOLLEDIDWITH")
     if total_collisions > 0:
         return True
     else:
         return False
    
    def parralax(self , x , y):
        self.x = self.x * x
        self.y = self.y * y
#####functions##########
def playermove_shadow(x , y):
 global velx
 global vely
 velx = velx + x
 vely = vely + y
def playermove(x , y):
 global playerx
 global playery
 global floor
 global velx
 global vely
 global epc
 playerxbackup = playerx
 playerybackup = playery
 playerx = playerx  + - x
 playery = playery +  - y
 epc = collisionbox( playerx -5   , playery - 30  , playerx + 5  , playery + -10  )
 if not   collisioncheckworld(epc):
  jump = 0
  for ActiveSprite in range(-1, len(SRAM) - 1):
   if SRAM[ActiveSprite].mode == "world" :
    SRAM[ActiveSprite].x = x ##SRAM[1].x + x
    SRAM[ActiveSprite].y = y ##SRAM[1].y + y
    x = SRAM[ActiveSprite].x
    y = SRAM[ActiveSprite].y
    exec(SRAM[ActiveSprite].code)
    SRAM[ActiveSprite].move()
    if debug == 1 :
        print( playerx) 
        print(playery)
 else:
     jump = 1
     playerx = playerxbackup
     playery = playerybackup
 if not  x == 0:
  velx = 0
 if not y == 0:
  vely = 0
 

#####sprite class wrapper
def  gameworldadd(name , texture , x , y ):
  t = len(SRAM)
  SRAM.append(name)
  ActiveSprite = t
  SRAM[t] = sprite(texture , x , y , "world")
def NewActiveSprite(name , texture , x , y ,code):
  t = len(SRAM)
  SRAM.append(name)
  ActiveSprite = t
  SRAM[t] = sprite(texture , x , y , "world")
  SRAM[t].code = code
  SRAM[t].name = name
  
 
def  hudadd(name , texture , x , y ):
 t = len(SRAM)
 SRAM.append(name)
 ActiveSprite = t
 SRAM[t] = sprite(texture , x , y , "HUD")


##input handler    
def input_handler(event):
 temp = event.keysym
 if temp == "Up"  and collisioncheckworld :
     playermove_shadow( 0 ,speed_Y )
 if temp == 'Down':
     playermove_shadow(0 , 0 - speed_Y)
 if temp == 'Left':
     playermove_shadow(speed_X , 0)
 if temp == 'Right':
     playermove_shadow( 0 - speed_X , 0)
 
## collision handler x axis
def xcollision_handler(co1 , co2):
 if (co1.xf > co2.xf and co1.xf < co2.x1) or (co1.x1 > co2.xf and co1.x1 <co2.x1) or (co2.xf > co1.xf and co2.xf < co1.x1) or  (co2.x1 > co1.xf and co2.x1 < co1.x1):
  return True
 else:
  return False
## collision handler y ayis
def ycollision_handler(co1 , co2):
 if (co1.yf > co2.yf and co1.yf < co2.y1) or (co1.y1 > co2.yf and co1.y1 <co2.y1) or (co2.yf > co1.yf and co2.yf < co1.y1) or  (co2.y1 > co1.yf and co2.y1 < co1.y1):
  return True
 else:
  return False
## collision check
def collisioncheck(c1 , c2):
 if xcollision_handler(c1 ,c2) and ycollision_handler(c1 , c2):
  return True
 else:
  return False
def find_player_coords():
     global syncx
     global syncy
     syncx = ( 0 -(SRAM[1].xtrack - 500)) 
     syncy = (0 - (SRAM[1].ytrack - 500))
def generate_player_collbox() :
 global all 
 playercollbox = collisionbox( playerx -5 , playery - 5 , playerx + 5 , playery + 5 )
def collisioncheckworld(thing):
 global colliders
 total_collisions = 0
 global collided
 collided = []
 for ActiveSprite in range(-1, len(colliders) - 1):
    if  collisioncheck(thing , colliders[ActiveSprite]):
         total_collisions = total_collisions + 1
         exec(colliders[ActiveSprite].code)
         collided.append(str(colliders[ActiveSprite].name))
 if total_collisions > 0:
     return True
 else:
     return False
   
def collider_add(item) :
    global colliders
    colliders.append(item)

#### sprite loader
def load_sprite(sprite):
 NewActiveSprite(sprite[0],sprite[1],sprite[2],sprite[3],sprite[4])
###level loader
def load_level(level): ##### assumes  that the level does not contain Illegitimate code
 if exists(level):
  exec(open(level, "r").read(),globals())
############################### code that needs to run bevore loop
#####colliders #########
canvas.bind_all('<KeyPress>' , input_handler)
# floor = collisionbox(-160 , 90 , 170 , 100)
# collider_add(floor)
# wall_1 = collisionbox(-170,-26,-160,100)
# wall_2 = collisionbox(170,-30,180,100)
# wall_2.code = "print('touched wall2')"
# collider_add(wall_1)
# collider_add(wall_2)
# ########game graphics #########
# NewActiveSprite("background" , "background.png" , 250 , 242 , "SRAM[ActiveSprite].parralax(0.9 ,1)")
# 
# gameworldadd("mansion" , "mansion.png" , 250 , 250)
# NewActiveSprite("ghost" , "test.png" , 250 , 252 , "SRAM[ActiveSprite].ghost()")
# 
# hudadd('character' , 'character.png' , 250 , 250)

load_level("levels/default")
canvas.addtag_all("all")
canvas.scale("all",250,250,20,20)

########################### main loop ############
while True:
 win.update()
 if vely > 20:
  vely = 10
 elif vely < -10:
  vely = -10
  
 if velx > 10:
     velx = 10
 elif velx < -10:
     velx = -10
     
 playermove(0 , vely)
 playermove(velx , 1)
 vely = vely + - 2
 if debugtool == 1 :
    velx = -1
    io = input("enterto continue")
 


map = [[-10, 10 , "chair.png" , "trash = 0"],[],[],]



 


